**Guia 5 Unidad 2**                                                                                            
Programa basado en programación orientada a objetos y arboles binarios de busqueda y arboles binarios balanceados.                                                                           

**Pre-requisitos**                                                                                                                                    
Cplusplus                                                                                                                                                
Make                                                                                                                                      
Ubuntu                                                                                                                                                                                     
graphviz                                                                                                                                                                           
**Instalación**                                                                                                                                       
-Instalar Make si no esta en su computador.                                

Para instalar Make inicie la terminal y escriba lo siguiente:

sudo apt install make.

-Instalar graphviz si no esta en su computador.

Para instalar graphviz inicie la terminal como super usuario y escriba lo siguiente:

apt-get install graphviz.


**Problematica**                                                                                                                                       
Reescribir un programa que esta en c a c++ y modificarlo para lograr hacer un arbol binario de busqueda que contenga IDs de proteinas, este debera poder eliminar, insertar o modificar IDs y ademas el usuario podra visualizar el arbol.                            

**Ejecutando**                                                                                                                                         
Para abrir el programa necesita ejecutar el archivo make desde la terminal, luego escribir en la terminal lo siguiente: ./programa.                    
AL abrir el programa este le pedira ingresar la raiz del arbol, una vez ingresada visualizara la imagen del arbol y un menu con 6 opciones.

La primera opcion le permite insertar una una nueva ID al arbol y luego mostrara el arbol con la ID añadida, la segunda opcion le permite buscar un ID en el arbol y le dira si esta en el arbol o no, la tercera le permite modificar un ID(eliminarlo y luego insertar otro), la cuarta opcion le permite eliminar un ID del arbol y le mostrara el arbol ordenado, la quinta le mostrara el arbol ordenado y finalmente la septima opcion le sexta salir del programa.

                                                                                            
**Construido con**                                                                                                                                    
C++                                                                                                                                      
                                                                                                                                         
Librerias:                                                                                                                               
Iostream                                                                                                                              
Fstream                                                                                                                                                  
String

**Versionado**                                                                                                                                        
Version 1.20                                                                                                                                        

**Autores**                                                                                                                                                                                                                                                 
Rodrigo Valenzuela                                                                                                                                                                                               