#include <fstream>
#include <iostream>
using namespace std;

//* estructura del nodo */
typedef struct _NODO {
  struct _NODO *izq;
  struct _NODO *der;
  string info;
  int FE;
} NODO;
