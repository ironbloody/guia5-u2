#include <fstream>
#include <iostream>
#include <string>
#include <cstring>
#include "CrearGrafo.h"
using namespace std;

Grafo::Grafo() {}
ofstream fp;

void Grafo::GenerarGrafo(NODO *ArbolInt) {
  // Se abre el archivo.
  fp.open ("grafo.txt");
  
  fp << "digraph G {" << endl;
  fp << "node [style=filled fillcolor=yellow];" << endl;
  
  fp << "nullraiz [shape=point];" << endl;
  fp << "nullraiz->" << "\"" << ArbolInt->info << "\"" << " ";
  fp << "[label=" << ArbolInt->FE << "];" << endl;
  PreOrden(ArbolInt);
  
  fp << "}";
  
  // Se cierra el archivo.
  fp.close();
  
  // Se genera el grafo.
  system("dot -Tpng -ografo.png grafo.txt");
  
  // Visualizacion del grafo.
  system("eog grafo.png &");
}

/* */
void Grafo::PreOrden(NODO *p) {
  if (p != NULL) {
    if (p->izq != NULL) {
      fp << "\"" <<  p->info << "\"" << "->" << "\"" << p->izq->info << "\"" << "[label=" << p->izq->FE << "];" << endl;
    } else{
      fp << "\"" << p->info << "i\"" << " [shape=point];" << endl;
      fp << "\"" << p->info << "\"" << "->" << "\"" << p->info << "i\"" << ";" << endl;
    }
    if (p->der != NULL) { 
      fp << "\"" << p->info << "\"" << "->" << "\"" << p->der->info << "\"" << "[label=" << p->der->FE << "];" << endl;
    } else{
      fp << "\"" << p->info << "d\"" << " [shape=point];" << endl;
      fp << "\"" << p->info << "\"" << "->" << "\"" << p->info << "d\"" << ";" << endl;
    }
    PreOrden(p->izq);
    PreOrden(p->der); 
  }
}
