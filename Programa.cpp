#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "CrearGrafo.h"

#define TRUE 1
#define FALSE 0

// Prototipo.
void InsercionBalanceado(NODO *&nodocabeza, int *BO, string infor);
void Busqueda(NODO *nodo, string infor);
void Restructura1(NODO **nodocabeza, int *BO);
void Restructura2(NODO **nodocabeza, int *BO);
void Borra(NODO **aux1, NODO **otro1, int *BO);
void EliminacionBalanceado(NODO **nodocabeza, int *BO, string infor);
int Menu();

// Funcion para insertar nodos.
void InsercionBalanceado(NODO *&nodocabeza, int *BO, string infor) {
  // Creacion de variables.
  NODO *nodo = NULL;
  NODO *nodo1 = NULL;
  NODO *nodo2 = NULL; 
  
  // Creacion temporal.
  nodo = nodocabeza;
  
  // Si el nodo es distinto de NULL.
  if (nodo != NULL) {
    
    // Si la comparacion entre nodos es igual a -1
    if (infor.compare(nodo->info) == -1) {
      // Recursividad para recorrer el arbol por la izquierda
      InsercionBalanceado((nodo->izq), BO, infor);
      
      // Si el BO es igual a TRUE.
      if(*BO == TRUE) {
        // Condiciones para FE.
        switch (nodo->FE){
          // Si el nodo apuntando a FE es 1.
          case 1: 
            // El nodo apuntando a FE se iguala 0.
            nodo->FE = 0;
            // BO sera FALSE.
            *BO = FALSE;
            break;
          
          // Si el nodo apuntando a FE es 0.
          case 0: 
            // El nodo apuntando a FE se iguala a -1.
            nodo->FE = -1;
            break;
            
          // Si el nodo apuntando a FE es -1.
          case -1: 
            // reestructuración del árbol.
            nodo1 = nodo->izq;
            
            // Rotacion II .
            // Si el nodo1 apuntando a FE es menor o igual a 0.
            if (nodo1->FE <= 0) { 
              // Se iguala el nodo apuntando izquierda con el nodo1 apuntando a derecha.
              nodo->izq = nodo1->der;
              // Nodo1 apuntando a derecha se iguala al nodo.
              nodo1->der = nodo;
              // Nodo apuntando a FE se iguala a 0.
              nodo->FE = 0;
              // Y el nodo se iguala al nodo1.
              nodo = nodo1;
            
            } else { 
              // Rotacion ID. 
              // un nuevo nodo2 se igualara al nodo1 apuntando a la derecha.
              nodo2 = nodo1->der;
              // Nodo apuntando al izquierda se iguala al nodo2 apuntando a la derecha.
              nodo->izq = nodo2->der;
              // Nodo2 apuntando a la derecha se iguala a nodo.
              nodo2->der = nodo;
              // Nodo1 apuntando a derecha se iguala a nodo2 apuntando a la izquierda.
              nodo1->der = nodo2->izq;
              // Nodo2 apuntando a izquierda se iguala al nodo1
              nodo2->izq = nodo1;
              
              // Si el nodo2 apuntando a FE es igual a -1
              if (nodo2->FE == -1)
                // El nodo apuntando a FE se iguala a 1.
                nodo->FE = 1;
              // Sino
              else
                // Nodo apuntando a FE se iguala a 0.
                nodo->FE =0;
              
              // Si el nodo2 apuntando a FE es igual a 1.
              if (nodo2->FE == 1)
                // El nodo1 apuntando a FE es igual a -1.
                nodo1->FE = -1;
              // Sino.
              else
                // El nodo1 apuntando a FE se iguala a 0.
                nodo1->FE = 0;
              
              // El nodo se iguala a nodo2.
              nodo = nodo2;
            }
            
            // Nodo apuntando a FE se iguala a 0.
            nodo->FE = 0;
            // BO se iguala a FALSE.
            *BO = FALSE;
            break;
        }
      } 
      
    // Sino.
    } else {
      
      // Si la comparacion de nodos es igual a 1.
      if (infor.compare(nodo->info) == 1) {
        // Recursividad para recorrer el arbol por la izquierda.
        InsercionBalanceado((nodo->der), BO, infor);
        
        // Si el BO es igual a TRUE.
        if (*BO == TRUE) {
          
          // Casos para el nodo apuntado a FE.
          switch (nodo->FE) {
            
            // Caso si nodo apuntando a FE es igual a -1.
            case -1: 
              // Nodo apuntando a FE se iguala 0.
              nodo->FE = 0;
              // Y BO se iguala a FALSE.
              *BO = FALSE;
              break;
              
            // Caso si el nodo apuntando a FE es igual a 0.
            case 0: 
              // Nodo apuntando a FE se iguala a 1.
              nodo->FE = 1;
              break;
          
            // Caso si el nodo apuntando a FE es igual a 1.
            case 1: 
              // Reestructuración del árbol.
              nodo1 = nodo->der;
              
              // Si el nodo1 apuntando a FE es mayor o igual a 0.
              if (nodo1->FE >= 0) { 
                // Rotacion DD.
                // Nodo apuntando a derecha se iguala a nodo1 apuntando a izquierda.
                nodo->der = nodo1->izq;
                // Nodo1 apuntando a izquierda es iguala a nodo.
                nodo1->izq = nodo;
                // Nodo apuntando a FE se iguala a 0.
                nodo->FE = 0;
                // Nodo se iguala a nodo1.
                nodo = nodo1;
                
              // Sino.
              } else { 
                // Rotacion DI.
                // Se nodo2 se iguala a nodo1 apuntando a izquierda.
                nodo2 = nodo1->izq;
                // Nodo apuntando a derecha se iguala a nodo2 apuntando izquierda.
                nodo->der = nodo2->izq;
                // Nodo2 apuntando a izquierda se iguala a nodo.
                nodo2->izq = nodo;
                // Nodo1 apuntando a izquierda se iguala a nodo2 apuntando a derecha.
                nodo1->izq = nodo2->der;
                // Nodo2 apuntando a derecha se iguala a nodo1.
                nodo2->der = nodo1;
                
                // Si nodo2 apuntando a FE es igual a 1.
                if (nodo2->FE == 1)
                  // Nodo apuntando a FE se iguala a -1.
                  nodo->FE = -1;
                // Sino.
                else
                  // Nodo apuntando a FE se iguala a 0.
                  nodo->FE = 0;
                
                // Si el nodo2 apuntando a FE es igual a -1.
                if (nodo2->FE == -1)
                  // El nodo1 apuntando a FE se iguala a 1.
                  nodo1->FE = 1;
                // Sino.
                else
                  // El nodo1 apuntando a FE es igual a 0.
                  nodo1->FE = 0;
   
                // El nodo se iguala a nodo2.
                nodo = nodo2;
              }
              
              // Nodo apuntando a FE se iguala a 0.
              nodo->FE = 0;
              // BO se iguala a FALSE.
              BO = FALSE;
              break;
          }
        }
      // Sino.
      } else {
        cout << "El nodo ya se encuentra en el árbol\n" << endl;
      }
    }
  // Sino.
  } else {
    
    nodo = new NODO();
    nodo->izq = NULL;
    nodo->der = NULL;
    nodo->info = infor;
    nodo->FE = 0;
    *BO = TRUE;
  }
  
  nodocabeza = nodo;
}

// Funcion para buscar.
void Busqueda(NODO *nodo, string infor) {
  // Si el nodo es distinto de NULL.
  if (nodo != NULL) {
    // Si infor es menor que el nodo apuntando a info.
    if (infor < nodo->info) {
      // Recursividad para recorrer el arbol por la izquierda.
      Busqueda(nodo->izq,infor);
    // Sino.
    } else {
      // Si infor es mayor al nodo apuntando a info.
      if (infor > nodo->info) {
        // Recursividad para recorrer el arbol por la derecha.
        Busqueda(nodo->der,infor);
      // Sino.
      } else {
        cout << "El nodo SI se encuentra en el árbol\n" << endl;
      }
    }
  // Sino.
  } else {
    cout << "El nodo NO se encuentra en el árbol\n" << endl;
  }
}

// Funcion para reestructurar.
void Restructura1(NODO **nodocabeza, int *BO) {
  // Creacion de variables.
  NODO *nodo, *nodo1, *nodo2; 
  nodo = *nodocabeza;
  
  // Si el BO es igual a TRUE.
  if (*BO == TRUE) {
    // Condiciones para el nodo apuntando a FE.
    switch (nodo->FE) {
      // Si el nodo apuntando a FE es -1.
      case -1: 
        // Se iguala a 0.
        nodo->FE = 0;
        break;
        
      // Si el nodo apuntando a FE es 0.
      case 0: 
        // SE iguala a -1.
        nodo->FE = 1;
        // BO se iguala a FALSE.
        *BO = FALSE;
        break;
   
   // Si el nodo apuntando a FE es 1.
    case 1: 
      // Reestructuracion del árbol.
      nodo1 = nodo->der;
      
      // Si el nodo1 apuntando a FE es mayor o igual a 0.
      if (nodo1->FE >= 0) { 
        // rotacion DD 
        nodo->der = nodo1->izq;
        nodo1->izq = nodo;
        
        // Casos para el nodo1 apuntando a FE.
        switch (nodo1->FE) {
        // Caso si nodo1 apuntando a FE es 0.
          case 0: 
            // Nodo apuntando a FE se iguala a 1.
            nodo->FE = 1;
            // Nodo1 apuntandoa FE se iguala a -1.
            nodo1->FE = -1;
            // BO se iguala a FALSE.
            *BO = FALSE;
            break;
          // Caso si nodo1 apuntando a FE es 1.
          case 1: 
            // Nodo apuntando a FE se iguala a 0.
            nodo->FE = 0;
            // Nodo1 apuntando a FE se iguala a 0.
            nodo1->FE = 0;
            // BO se iguala a FALSE.
            *BO = FALSE;
            break;           
        }
        nodo = nodo1;
      // Sino.
      } else { 
        // Rotacion DI.
        // Nodo2 se iguala al nodo1 apuntando a izquierda.
        nodo2 = nodo1->izq;
        // Nodo apuntando a derecha se iguala a nodo2 apuntando a izquierda.
        nodo->der = nodo2->izq;
        // Nodo2 apuntando a izquierda se iguala a nodo.
        nodo2->izq = nodo;
        // Nodo1 apuntando a izquierda se iguala anodo2 apuntando a derecha.
        nodo1->izq = nodo2->der;
        // nodo2 aputando a dereceha se iguala nodo1.
        nodo2->der = nodo1;
       
       // Si el nodo2 apuntando a FE es igual a 1.
        if (nodo2->FE == 1)
          // El nodo apuntando a FE se iguala a -1.
          nodo->FE = -1;
        // Sino.
        else
          // El nodo apuntando a FE se iguala a 0.
          nodo->FE = 0;
        
        // Si el nodo2 apuntando a FE es igual a -1.
        if (nodo2->FE == -1)
          // El nodo1 apuntando a FE se iguala a 1.
          nodo1->FE = 1;
        // Sino.
        else
          // El nodo1 apuntando a FE se iguala a 0.
          nodo1->FE = 0;
        
        nodo = nodo2;
        nodo2->FE = 0;       
      } 
      break;   
    }
  }
  *nodocabeza=nodo;
}

// Funcion para reestructurar.
void Restructura2(NODO **nodocabeza, int *BO) {
  // Creacion de variables.
  NODO *nodo, *nodo1, *nodo2; 
  nodo = *nodocabeza;
  
  // Si el BO es igual a TRUE.
  if (*BO == TRUE) {
    // Casos para el nodo apuntando a FE.
    switch (nodo->FE) {
      // Caso para cuando el nodo apuntando a FE es 1.
      case 1: 
        // Nodo apuntando a FE se iguala a 0.
        nodo->FE = 0;
        break;
      // Caso para cuando el nodo apuntando a FE es 0.
      case 0: 
        // Nodo apuntando a FE se iguala a -1.
        nodo->FE = -1;
        // BO se iguala a FALSE.
        *BO = FALSE;
        break;
      // Caso para cuando el nodo apuntando a FE es -1.
      case -1: 
        // Reestructuracion del árbol.
        nodo1 = nodo->izq;
        // Si el nodo1 apuntando a FE es menor o igual a 0.
        if (nodo1->FE<=0) { 
        
          // Rotacion II.
          // Nodo apuntando a izquierda se iguala nodo1 apuntando a derecha.
          nodo->izq = nodo1->der;
          // Nodo1 apuntando a derecha se iguala a nodo.
          nodo1->der = nodo;
          // Casos para el nodo1 apuntando a FE.
          switch (nodo1->FE) {
            // Si nodo1 apuntando a FE es 0.
            case 0: 
              // Nodo apuntando a FE se iguala a -1.
              nodo->FE = -1;
              // Nodo1 apuntando a FE se iguala a 1.
              nodo1->FE = 1;
              // BO se iguala a FALSE.
              *BO = FALSE;
              break;
            // Si nodo1 apuntando a FE es -1.
            case -1: 
              // Nodo apuntando a FE se iguala a ´0.
              nodo->FE = 0;
              // Nodo1 apuntando a FE se iguala a 0.
              nodo1->FE = 0;
              // BO se iguala a FALSE.
              *BO = FALSE;
              break;
          }
          nodo = nodo1;
        // Sino.
        } else { 
          // Rotacion ID.
          // Nodo2 se iguala a nodo 1 apuntando a derecha.
          nodo2 = nodo1->der;
          // Nodo apuntando a izq se iguala a nodo2 apuntando a derecha.
          nodo->izq = nodo2->der;
          // Nodo2 apuntando a derecha se iguala a nodo.
          nodo2->der = nodo;
          // Nodo1 apuntando derecha se iguala a nodo2 apuntando a izquierda.
          nodo1->der = nodo2->izq;
          // Nodo2 apuntando a izquierda se iugala a nodo1.
          nodo2->izq = nodo1;
       
          // Si el nodo2 apuntando a FE es igual a -1.
          if (nodo2->FE == -1)
            // Nodo apuntando a FE se iguala a 1.
            nodo->FE = 1;
          // Sino.
          else
            // Nodo apuntando a FE se iguala a 0.
            nodo->FE = 0;
        
          // Si nodo2 apuntando a FE es igual a 1.
          if (nodo2->FE == 1)
            // Nodo1 apuntando a FE se iguala a -1.
            nodo1->FE = -1;
          // Sino.
          else
            // Nodo1 apuntandoa FE se iguala a 0.
            nodo1->FE = 0;
      
          nodo = nodo2;
          nodo2->FE = 0;       
        }      
        break;   
    }
  }
  *nodocabeza = nodo;
}

// Complementa la eliminacion.
void Borra(NODO **aux1, NODO **otro1, int *BO) {
  // Creacion de variables.
  NODO *aux, *otro; 
  aux=*aux1;
  otro=*otro1;
  
  // Si el auxiliar apuntando a derecha es distinto de NULL.
  if (aux->der != NULL) {
    // Recursividad para recorrer el arbol por la derecha.
    Borra(&(aux->der),&otro,BO); 
    Restructura2(&aux,BO);
  // Sino.
  } else {
    // Otro apuntando a info se iguala a auxiliar apuntando a info.
    otro->info = aux->info;
    // Auxiliar se iguala a auxiliar apuntando a izquierda.
    aux = aux->izq;
    // BO se iguala a TRUE.
    *BO = TRUE;
  }
  *aux1=aux;
  *otro1=otro;
}

// Funcion para eliminar nodos.
void EliminacionBalanceado(NODO **nodocabeza, int *BO, string infor) {
  // Creacion de variables.
  NODO *nodo, *otro; 
  
  nodo = *nodocabeza;
  
  // Si el nodo es distinto de NULL.
  if (nodo != NULL) {
    // Si la comparacion de nodos es iguala a -1.
    if (infor.compare(nodo->info) == -1) {
      // Recursividad para recorrer el arbol por la izquierda.
      EliminacionBalanceado(&(nodo->izq),BO,infor);
      Restructura1(&nodo,BO);
    // Sino.
    } else {
      // Si la comparacion de nodos es iguala a 1.
      if (infor.compare(nodo->info) == 1) {
        // Recursividad para recorrer el arbol por la derecha
        EliminacionBalanceado(&(nodo->der),BO,infor);
        Restructura2(&nodo,BO); 
      // Sino.
      } else {
        // Se iguala otro con nodo.
        otro = nodo;
        // Si otro apuntando a derecha es igual a NULL.
        if (otro->der == NULL) {
          // Nodo se iguala a otro apuntando a izquierda.
          nodo = otro->izq;
          // BO se iguala a TRUE.
          *BO = TRUE;  
        // Sino.
        } else {
          // Si otro apuntando a izquierda es igual a NULL.
          if (otro->izq==NULL) {
            // Nodo se iguala a otro apuntando a derecha.
            nodo=otro->der;
            // BO se iguala a TRUE.
            *BO=TRUE;
          // Sino.
          } else {
            // SE llama a la funcion borrar pero por la izquierda.
            Borra(&(otro->izq),&otro,BO);
            Restructura1(&nodo,BO);
            otro = NULL;
          }
        }
      }
    } 
  // Sino.
  } else {
    cout << "El nodo NO se encuentra en el árbol\n" << endl;
  }
  *nodocabeza=nodo;
}

//Función principal
int main(int argc, char **argv) {
  // Creacion de variables.
  int opcion;
  string elemento, linea, nuevo;
  NODO *raiz = NULL;
  Grafo g = Grafo();
  
  system("clear");
  opcion = Menu();
  int inicio;
  // Recorre el archivo de texto e inserta los nodos.
  ifstream file("pdbs.txt");
  while(getline(file,linea)){
    inicio = FALSE;
    InsercionBalanceado(raiz, &inicio, linea);
  }
  
  // Menu.
  while (opcion) {
    
    switch(opcion) {
    case 1:
      cout << "Ingresar elemento: " << endl;
      cin >> elemento;
      inicio=FALSE;
      // Se llama a la insercion.
      InsercionBalanceado(raiz, &inicio, elemento);
      // Se genera el grafo.
      g.GenerarGrafo(raiz);
      break;
      
    case 2:
      cout << "Buscar elemento: " << endl;
      cin >> elemento;
      // Se llama a busqueda.
      Busqueda(raiz, elemento);
      break;
      
    case 3:
      cout << "Inserte valor viejo a eliminar: " << endl;
      cin >> elemento;
      inicio=FALSE;
      // Se llama a eliminacion.
      EliminacionBalanceado(&raiz, &inicio, elemento);
      // Se genera el grafo.
      cout << "Inserte el valor nuevo a insertar: " << endl;
      cin >> nuevo;
      InsercionBalanceado(raiz, &inicio, nuevo);
      g.GenerarGrafo(raiz);
      break;
      
    case 4:
      cout << "Eliminar elemento: " << endl;
      cin >> elemento;
      inicio=FALSE;
      // Se llama a eliminacion.
      EliminacionBalanceado(&raiz, &inicio, elemento);
      // Se genera el grafo.
      g.GenerarGrafo(raiz);
      break;
      
    case 5:
      // Se genera el grafo.
      g.GenerarGrafo(raiz);
      break;
      
    case 0: 
      exit(0);
    }
    
    opcion = Menu();
  }
  
  return 0;
}

// Muestra un menu de opciones.
int Menu() {
  // Creacion de variable.
  int Op;
  
  do {
    cout << "--------------------" << endl;
    cout << "1) Insertar" << endl;
    cout << "2) Buscar" << endl;
    cout << "3) modificar" << endl;
    cout << "4) Eliminacion" << endl;
    cout << "5) Grafo" << endl;
    cout << "0) Salir" << endl;
    cout << "Opción: " << endl;
    cin >> Op;
  } while (Op<0 || Op>7);
  
  return (Op);
}