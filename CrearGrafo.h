#include <fstream>
#include <iostream>
#include "Programa.h"
using namespace std;

class Grafo {
  private:
  public:  
    // Constructor.
    Grafo();
    // Funciones.
    void GenerarGrafo(NODO *ArbolInt);
    void PreOrden(NODO *p);
};